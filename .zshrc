zmodload zsh/zprof
# oh-my-zsh
export ZSH="/home/qburst/.oh-my-zsh"
ZSH_THEME="agnoster"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git rails vi-mode sudo)

source $ZSH/oh-my-zsh.sh

# User configuration

# Set $PATH variable
export PATH="${PATH}:$HOME/bin:$HOME/.local/bin:/home/qburst/.rvm/gems/ruby-2.6.3/bin"

#Base 16
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# Defaults
DEFAULT_USER=$USER

# Personal Alias
# nvim
if [ -x "$(command -v nvim)" ]; then
	alias vi="nvim"
else
	alias vi="vim"
fi

# Xomodmap
setxkbmap -option caps:swapescape
# xkbcomp -I$HOME/.xkb ~/.xkb/keymap/mykbd $DISPLAY

# npm
export PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules

# git
alias gitlog="watch --color -n 3 git log --all --decorate --oneline --graph --color=always"
alias gitstatus="watch --color -n 3 git -c color.status=always status --short"


# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

# sonar integrations
export PATH=${PATH}:~/dev/bin/sonar-runner-2.4/bin
export SONAR_RUNNER_HOME=~/dev/bin/sonar-runner-2.4/
